<?php

use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_roles')->insert([
            'levelCode' => 'SA',
            'levelName' => 'Super Admin'
        ]);
        DB::table('user_roles')->insert([
            'levelCode' => 'AD',
            'levelName' => 'Admin'
        ]);
        DB::table('user_roles')->insert([
            'levelCode' => 'CC',
            'levelName' => 'Contact Center'
        ]);
        DB::table('user_roles')->insert([
            'levelCode' => 'CH',
            'levelName' => 'Chairman'
        ]);

    }
}
