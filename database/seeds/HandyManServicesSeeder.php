<?php

use Illuminate\Database\Seeder;

class HandyManServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('handy_man_services')->insert([
            'srCode' => 'ELTC',
            'srName' => 'Electrician',
            'srImg' => 'electrician'
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'PLMB',
            'srName' => 'Plumber',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'ACFD',
            'srName' => 'AC / Fridge Repair',
            'srImg' => '',
            'activeStatus' => false
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'CVWK',
            'srName' => 'Civil Works',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'PNTR',
            'srName' => 'Painter',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'FABR',
            'srName' => 'Fabricator',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'CRPR',
            'srName' => 'Carpenter',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'PSCT',
            'srName' => 'Pest Control',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'DRVR',
            'srName' => 'Driver',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'LPDK',
            'srName' => 'Laptop / Desktop',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'LNSD',
            'srName' => 'Laundry Service',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'INHD',
            'srName' => 'Interior and Home Decor',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'GRDN',
            'srName' => 'Gardner',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'BRMN',
            'srName' => 'Borewell Maintenance',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'HNBT',
            'srName' => 'Health and Beauty',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'EVMG',
            'srName' => 'Event Management',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'TRBK',
            'srName' => 'Travel Booking',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'ELRP',
            'srName' => 'Electronic Repair',
            'srImg' => ''
        ]);
        DB::table('handy_man_services')->insert([
            'srCode' => 'OTSR',
            'srName' => 'Other Services',
            'srImg' => ''
        ]);

    }
}
