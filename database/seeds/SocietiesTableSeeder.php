<?php

use Illuminate\Database\Seeder;

class SocietiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('societies')->insert([
            'socCode' => 'NAVN',
            'socName' => 'Navjeevan Society',
            'socRegNo' => '39767',
            'socAddress' => 'Borivali East, Near Metro Cineplex, Mumbai - 200027',
            'socGSTNo' => 'GSTNO33564',
            'socCTSNo' => 'CTSNO33564',
            'socCity' => 'Mumbai',
            'socState' => 'Maharashtra',
            'socPin' => '200027',
            'socEmail' => 'admin@navjeenvan.com',
            'socContact' => '9912121234',
            'socTenantCnt' => '40',
            'socResFlatsCnt' => '35',
            'socShopsCnt' => '5'
        ]);
        DB::table('societies')->insert([
            'socCode' => 'BHAR',
            'socName' => 'Bharat Society',
            'socRegNo' => '67023',
            'socAddress' => 'Malad West, Near Jantra Market, Mumbai - 400027',
            'socGSTNo' => 'GSTNO74564',
            'socCTSNo' => 'CTSNO74564',
            'socCity' => 'Mumbai',
            'socState' => 'Maharashtra',
            'socPin' => '400027',
            'socEmail' => 'admin@bharatsoc.com',
            'socContact' => '9912121774',
            'socTenantCnt' => '57',
            'socResFlatsCnt' => '50',
            'socShopsCnt' => '7'
        ]);
        DB::table('societies')->insert([
            'socCode' => 'DASH',
            'socName' => 'Dashrat Society',
            'socRegNo' => '77502',
            'socAddress' => 'Vasat West, Near Dreams Mall, Mumbai - 401208',
            'socGSTNo' => 'GSTNO99302',
            'socCTSNo' => 'CTSNO99054',
            'socCity' => 'Palghar',
            'socState' => 'Maharashtra',
            'socPin' => '401208',
            'socEmail' => 'admin@dreams.com',
            'socContact' => '9920657768',
            'socTenantCnt' => '40',
            'socResFlatsCnt' => '35',
            'socShopsCnt' => '5'
        ]);
    }
}
