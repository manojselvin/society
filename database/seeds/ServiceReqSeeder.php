<?php

use Illuminate\Database\Seeder;

class ServiceReqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('service_reqs')->insert([
            'memID' => 'AIRBHARB401',
            'srCode' => 'ELTC',
            'reqMsg' => 'Get me an electrician',
            'timeSlot' => '24th May 2018 : 3pm to 5pm',
            'wrkCode' => '',
            'ccTeamComments' => '',
            'reqFeedBack' => ''
        ]);
        DB::table('service_reqs')->insert([
            'memID' => 'AIRBHARB401',
            'srCode' => 'EVMG',
            'reqMsg' => 'Get me an event guy',
            'timeSlot' => '2th October 2018 : 1pm to 9pm',
            'wrkCode' => '',
            'ccTeamComments' => '',
            'reqFeedBack' => ''
        ]);
        DB::table('service_reqs')->insert([
            'memID' => 'NIJIDASHC608',
            'srCode' => 'EVMG',
            'reqMsg' => 'Get me an event guy',
            'timeSlot' => '2th October 2018 : 1pm to 9pm',
            'wrkCode' => '',
            'ccTeamComments' => '',
            'reqFeedBack' => ''
        ]);
        DB::table('service_reqs')->insert([
            'memID' => 'YUNUNAVNA101',
            'srCode' => 'PLMB',
            'reqMsg' => 'Get me an plumber',
            'timeSlot' => '24th May 2018 : 3pm to 5pm',
            'wrkCode' => '',
            'ccTeamComments' => '',
            'reqFeedBack' => ''
        ]);
    }
}
