<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('members')->insert([
            'memID' => 'AIRBHARB401', // Combination of 4 letters of name / SocID/ Flat No - All Caps - remove space/chars
            'memName' => 'Airi Satou',
            'memSocID' => 'BHAR',
            'memRoleID' => 'AC',
            'memWingNo' => 'B',
            'memFlatNo' => 'B-401',
            'memFloorNo' => '4',
            'memFlatType' => 'Residential',
            'memFlatUnitType' => '1 BHK',
            'memCarpetArea' => '580',
            'memNoOfOccupants' => '3',
            'memEmail' => 'airi@satou.com',
            'memContact' => '9989787656',
            'memDOB' => '25/09/1990',
            'memPAN' => 'BDNPO776B',
            'memAdhaar' => '9908674800931176',
            'memProf' => 'Service',
            'memOrgName' => 'Kranti Tech Ltd.',
            'memWorkEmail' => 'airi@kranti.com', // Why ?
            'memOffContact' => '02226607878',
            'memOffAddr' => 'Kamla Mills, Lower Parel, Off Sind Road, Nagpur, Maharashtra',
            'memOffCity' => 'Nagpur',
            'memOffState' => 'Maharashtra',
            'memOffPinCode' => '400178',
            'memFamilyDetails' => '{"family":[{"name":"Catherine Ferns","email":"catherine@ferns.com","phone":"99892525267","relation":"wife"},{"name":"Darrel Ferns","email":"darrel@ferns.com","phone":"9989252577","relation":"son"},{"name":"Brenda Fers","email":"brendra@ferns.com","phone":"99892445267","relation":"daughter"}]}', // Be a json of elemnts
            'memVehicleInfo' => '{"vehicle":[{"model":"Honda Activa","regNo":"MH 07 PV 9940","parkingNo":""},{"model":"Renault Duster","regNo":"MH 08 DD 7789","parkingNo":"C-401"}]}' // Be a json of elemnts
        ]);
        DB::table('members')->insert([
            'memID' => 'NIJIDASHC608', // Combination of 4 letters of name / SocID/ Flat No - All Caps - remove space/chars
            'memName' => 'Nijil Kumar',
            'memSocID' => 'DASH',
            'memRoleID' => 'TR',
            'memWingNo' => 'B',
            'memFlatNo' => 'C-608',
            'memFloorNo' => '4',
            'memFlatType' => 'Residential',
            'memFlatUnitType' => '1 BHK',
            'memCarpetArea' => '580',
            'memNoOfOccupants' => '3',
            'memEmail' => 'airi@satou.com',
            'memContact' => '9989787656',
            'memDOB' => '25/09/1990',
            'memPAN' => 'BDNPO776B',
            'memAdhaar' => '9908674800931176',
            'memProf' => 'Service',
            'memOrgName' => 'Kranti Tech Ltd.',
            'memWorkEmail' => 'airi@kranti.com', // Why ?
            'memOffContact' => '02226607878',
            'memOffAddr' => 'Kamla Mills, Lower Parel, Off Sind Road, Nagpur, Maharashtra',
            'memOffCity' => 'Nagpur',
            'memOffState' => 'Maharashtra',
            'memOffPinCode' => '400178',
            'memFamilyDetails' => '{"family":[{"name":"Catherine Ferns","email":"catherine@ferns.com","phone":"99892525267","relation":"wife"},{"name":"Darrel Ferns","email":"darrel@ferns.com","phone":"9989252577","relation":"son"},{"name":"Brenda Fers","email":"brendra@ferns.com","phone":"99892445267","relation":"daughter"}]}', // Be a json of elemnts
            'memVehicleInfo' => '{"vehicle":[{"model":"Honda Activa","regNo":"MH 07 PV 9940","parkingNo":""},{"model":"Renault Duster","regNo":"MH 08 DD 7789","parkingNo":"C-401"}]}' // Be a json of elemnts
        ]);
        DB::table('members')->insert([
            'memID' => 'YUNUNAVNA101', // Combination of 4 letters of name / SocID/ Flat No - All Caps - remove space/chars
            'memName' => 'Yunus Mehra',
            'memSocID' => 'NAVN',
            'memRoleID' => 'AC',
            'memWingNo' => 'A',
            'memFlatNo' => 'A-101',
            'memFloorNo' => '4',
            'memFlatType' => 'Residential',
            'memFlatUnitType' => '1 BHK',
            'memCarpetArea' => '580',
            'memNoOfOccupants' => '3',
            'memEmail' => 'airi@satou.com',
            'memContact' => '9989787656',
            'memDOB' => '25/09/1990',
            'memPAN' => 'BDNPO776B',
            'memAdhaar' => '9908674800931176',
            'memProf' => 'Service',
            'memOrgName' => 'Kranti Tech Ltd.',
            'memWorkEmail' => 'airi@kranti.com', // Why ?
            'memOffContact' => '02226607878',
            'memOffAddr' => 'Kamla Mills, Lower Parel, Off Sind Road, Nagpur, Maharashtra',
            'memOffCity' => 'Nagpur',
            'memOffState' => 'Maharashtra',
            'memOffPinCode' => '400178',
            'memFamilyDetails' => '{"family":[{"name":"Catherine Ferns","email":"catherine@ferns.com","phone":"99892525267","relation":"wife"},{"name":"Darrel Ferns","email":"darrel@ferns.com","phone":"9989252577","relation":"son"},{"name":"Brenda Fers","email":"brendra@ferns.com","phone":"99892445267","relation":"daughter"}]}', // Be a json of elemnts
            'memVehicleInfo' => '{"vehicle":[{"model":"Honda Activa","regNo":"MH 07 PV 9940","parkingNo":""},{"model":"Renault Duster","regNo":"MH 08 DD 7789","parkingNo":"C-401"}]}' // Be a json of elemnts
        ]);

        
    }
}
