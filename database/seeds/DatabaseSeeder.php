<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*-------------------------------------------------------------
        |   Run this command : composer dump-autoload prior to seeding 
        |   In case you face Class does not exist error
        ----------------------------------------------------------------*/
        $this->call([
            UserRolesTableSeeder::class,
            SocietiesTableSeeder::class,
            MembersTableSeeder::class,
            HandyManServicesSeeder::class,
            ServiceReqSeeder::class
        ]);
    }
}
