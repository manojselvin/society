<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceReqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_reqs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('memID',255);
            $table->string('srCode',255);
            $table->text('reqMsg');
            $table->string('timeSlot',255);
            $table->string('wrkCode',255)->default('');
            $table->text('ccTeamComments');
            $table->text('reqFeedBack');
            $table->boolean('activeStatus')->default(true);
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_reqs');
    }
}
