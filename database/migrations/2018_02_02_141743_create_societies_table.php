<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocietiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('societies', function (Blueprint $table) {
            $table->bigIncrements('id');
            // Short Code
            $table->string('socCode')->unique();
            $table->string('socName',255);
            $table->string('socRegNo',255);
            $table->text('socAddress');
            $table->string('socGSTNo',255);
            $table->string('socCTSNo',255);
            $table->string('socCity',255);
            $table->string('socState',255);
            $table->integer('socPin');
            $table->string('socEmail',255);
            $table->string('socContact',20);
            $table->integer('socTenantCnt');
            $table->integer('socResFlatsCnt');
            $table->integer('socShopsCnt');
            $table->boolean('activeStatus')->default(true);
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('societies');
    }
}
