<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHandyManServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('handy_man_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            // Short Code
            $table->string('srCode',255);
            $table->string('srName',255);
            $table->string('srImg',255);
            $table->boolean('activeStatus')->default(true);
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('handy_man_services');
    }
}
