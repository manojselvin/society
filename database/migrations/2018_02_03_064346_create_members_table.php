<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            // Short Code
            $table->string('memID')->unique();
            $table->string('memName',255);
            $table->string('memSocID',255);
            $table->string('memRoleID',255);
            $table->string('memWingNo',255);
            $table->string('memFlatNo',255);
            $table->string('memFloorNo',255);
            $table->string('memFlatType',255);
            $table->string('memFlatUnitType',255);
            $table->integer('memCarpetArea');
            $table->integer('memNoOfOccupants');
            $table->string('memEmail',255);
            $table->string('memContact',20);
            $table->string('memDOB',255);
            $table->string('memPAN',255);
            $table->string('memAdhaar',255);
            $table->string('memProf',255);
            $table->string('memOrgName',255);
            $table->string('memWorkEmail',255); // Why ?
            $table->string('memOffContact',255);
            $table->text('memOffAddr');
            $table->string('memOffCity',255);
            $table->string('memOffState',255);
            $table->string('memOffPinCode',255);
            $table->text('memFamilyDetails'); // Be a json of elemnts
            $table->text('memVehicleInfo'); // Be a json of elemnts
            $table->boolean('activeStatus')->default(true);
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
