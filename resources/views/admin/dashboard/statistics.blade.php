@extends('admin.layouts.master')

@push('dynamic_stylesheets')
<!-- morris chart -->
<link rel="stylesheet" href="{{ asset('assets/vendor_components/morris.js/morris.css') }}">

<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('assets/vendor_components/jvectormap/jquery-jvectormap.css') }}">
@endpush

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-12 col-md-6 col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-blue"><i class="ion ion-stats-bars"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">90<small>%</small></span>
                        <span class="info-box-text">Store Traffic</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-thumbsup"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">41,410</span>
                        <span class="info-box-text">User Likes</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-purple"><i class="ion ion-bag"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">760</span>
                        <span class="info-box-text">Monthly Sales</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="ion ion-person-stalker"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number">2,000</span>
                        <span class="info-box-text">Join Members</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Monthly Status</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <div class="btn-group">
                                <button type="button" class="btn  btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-wrench"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-8">
                                <p class="text-center">
                                    <strong>Sales: 1 Jan, 2017 - 30 Jul, 2017</strong>
                                </p>
                                <div class="chart">
                                    <!-- Sales Chart Canvas -->
                                    <canvas id="salesChart" style="height: 180px;"></canvas>
                                </div>
                                <!-- /.chart-responsive -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-12 col-lg-4">
                                <p class="text-center">
                                    <strong>Goal Completion</strong>
                                </p>
                                <div class="progress-group">
                                    <span class="progress-text">Add Products to Bag</span>
                                    <span class="progress-number"><b>140</b>/200</span>
                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-aqua" style="width: 70%"></div>
                                    </div>
                                </div>
                                <!-- /.progress-group -->
                                <div class="progress-group">
                                    <span class="progress-text">Complete Purchase</span>
                                    <span class="progress-number"><b>300</b>/400</span>
                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-red" style="width: 75%"></div>
                                    </div>
                                </div>
                                <!-- /.progress-group -->
                                <div class="progress-group">
                                    <span class="progress-text">Visit Page</span>
                                    <span class="progress-number"><b>400</b>/800</span>
                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-green" style="width: 50%"></div>
                                    </div>
                                </div>
                                <!-- /.progress-group -->
                                <div class="progress-group">
                                    <span class="progress-text">Send Inquiries</span>
                                    <span class="progress-number"><b>425</b>/500</span>
                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-yellow" style="width: 85%"></div>
                                    </div>
                                </div>
                                <!-- /.progress-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- ./box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-6 col-sm-3">
                                <div class="description-block border-right">
                                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                                    <h5 class="description-header">$3,249.43</h5>
                                    <span class="description-text">TOTAL REVENUE</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-6 col-sm-3">
                                <div class="description-block border-right">
                                    <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 70%</span>
                                    <h5 class="description-header">$2,376.90</h5>
                                    <span class="description-text">TOTAL COST</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-6 col-sm-3">
                                <div class="description-block border-right">
                                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 80%</span>
                                    <h5 class="description-header">$1,795.53</h5>
                                    <span class="description-text">TOTAL PROFIT</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-6 col-sm-3">
                                <div class="description-block">
                                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 28%</span>
                                    <h5 class="description-header">1800</h5>
                                    <span class="description-text">GOAL COMPLETIONS</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        {{--  <div class="row">
            <!-- Left col -->
            <div class="col-lg-12 col-xl-8">
                <div class="row">
                    <div class="col-lg-12 col-xl-6">
                        <!-- info box -->
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Browser Usage</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-8 col-md-12">
                                        <div class="chart-responsive">
                                            <canvas id="pieChart" height="150"></canvas>
                                        </div>
                                        <!-- ./chart-responsive -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-lg-4 col-md-12">
                                        <ul class="chart-legend clearfix">
                                            <li><i class="fa fa-circle-o text-purple"></i> Chrome</li>
                                            <li><i class="fa fa-circle-o text-green"></i> IE</li>
                                            <li><i class="fa fa-circle-o text-yellow"></i> FireFox</li>
                                            <li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
                                            <li><i class="fa fa-circle-o text-red"></i> Opera</li>
                                            <li><i class="fa fa-circle-o text-gray"></i> Navigator</li>
                                        </ul>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer no-padding">
                                <ul class="nav nav-pills d-block nav-stacked">
                                    <li class="nav-item"><a class="nav-link" href="#">USA
                                        <span class="pull-right text-red"><i class="fa fa-angle-up"></i> 12%</span></a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="#">India <span class="pull-right text-green"><i class="fa fa-angle-down"></i> 4%</span></a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="#">UK
                                        <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="#">Japan
                                        <span class="pull-right text-purple"><i class="fa fa-angle-right"></i> 18%</span></a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.footer -->
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-lg-12 col-xl-6">
                        <!-- PRODUCT LIST -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Recently Products</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <ul class="products-list product-list-in-box">
                                    <li class="item">
                                        <div class="product-img">
                                            <img src="images/default-50x50.gif" alt="Product Image">
                                        </div>
                                        <div class="product-info">
                                            <a href="javascript:void(0)" class="product-title">iphone 7plus
                                            <span class="label label-warning pull-right">$300</span></a>
                                            <span class="product-description">
                                            12MP Wide-angle and telephoto cameras.
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->
                                    <li class="item">
                                        <div class="product-img">
                                            <img src="images/default-50x50.gif" alt="Product Image">
                                        </div>
                                        <div class="product-info">
                                            <a href="javascript:void(0)" class="product-title">Apple Tv
                                            <span class="label label-info pull-right">$400</span></a>
                                            <span class="product-description">
                                            Library | For You | Browse | Radio
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->
                                    <li class="item">
                                        <div class="product-img">
                                            <img src="images/default-50x50.gif" alt="Product Image">
                                        </div>
                                        <div class="product-info">
                                            <a href="javascript:void(0)" class="product-title">MacBook Air<span
                                                class="label label-danger pull-right">$450</span></a>
                                            <span class="product-description">
                                            Make big things happen. All day long.
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->
                                    <li class="item">
                                        <div class="product-img">
                                            <img src="images/default-50x50.gif" alt="Product Image">
                                        </div>
                                        <div class="product-info">
                                            <a href="javascript:void(0)" class="product-title">iPad Pro
                                            <span class="label label-success pull-right">$289</span></a>
                                            <span class="product-description">
                                            Anything you can do, you can do better.
                                            </span>
                                        </div>
                                    </li>
                                    <!-- /.item -->
                                </ul>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-center">
                                <a href="javascript:void(0)" class="uppercase">View All Products</a>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <!-- MAP & BOX PANE -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Our Visitors</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-lg-9 col-md-8">
                                <div class="pad">
                                    <!-- Map will be created here -->
                                    <div id="visitfromworld" style="height: 325px;"></div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-lg-3 col-md-4">
                                <div class="row pad box-pane-right bg-blue m-0">
                                    <div class="col-4 col-sm-12 description-block margin-bottom p-0">
                                        <div class="sparkbar pad" data-color="#fff">80,60,95,70,75,80,50</div>
                                        <h5 class="description-header">7458</h5>
                                        <span class="description-text">Visits</span>
                                    </div>
                                    <!-- /.description-block -->
                                    <div class="col-4 col-sm-12 description-block margin-bottom p-0">
                                        <div class="sparkbar pad" data-color="#fff">70,40,85,70,61,93,63</div>
                                        <h5 class="description-header">56%</h5>
                                        <span class="description-text">Referrals</span>
                                    </div>
                                    <!-- /.description-block -->
                                    <div class="col-4 col-sm-12 description-block p-0">
                                        <div class="sparkbar pad" data-color="#fff">80,55,91,70,81,43,67</div>
                                        <h5 class="description-header">85%</h5>
                                        <span class="description-text">Organic</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">New Orders</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-responsive no-margin">
                                <thead>
                                    <tr>
                                        <th>Order No</th>
                                        <th>Item</th>
                                        <th>Status</th>
                                        <th>Popularity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">ODN84952</a></td>
                                        <td>Iphone 6s</td>
                                        <td><span class="label bg-purple">Shipped</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#7460ee" data-height="20">60,50,90,-40,91,-53,83</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">ODN84845</a></td>
                                        <td>Apple TV</td>
                                        <td><span class="label bg-yellow">Pending</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#f39c12" data-height="20">40,80,-90,80,61,-73,68</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">ODN84982</a></td>
                                        <td>Samsung TV</td>
                                        <td><span class="label bg-green">Delivered</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#41b613" data-height="20">60,50,90,-40,91,-53,83</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">ODN85452</a></td>
                                        <td>Intex Smart Watch</td>
                                        <td><span class="label bg-blue">Processing</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#389af0" data-height="20">40,80,-90,80,61,-73,68</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">ODN94992</a></td>
                                        <td>Onida AC</td>
                                        <td><span class="label bg-yellow">Pending</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#f39c12" data-height="20">40,80,-90,80,61,-73,68</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">ODN98952</a></td>
                                        <td>iPhone 7 Plus</td>
                                        <td><span class="label bg-green">Delivered</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#41b613" data-height="20">60,50,90,-40,91,-53,83</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">ODN88989</a></td>
                                        <td>Samsung LED</td>
                                        <td><span class="label bg-purple">Shipped</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#7460ee" data-height="20">60,50,90,-40,91,-53,83</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                        <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-lg-12 col-xl-4">
                <!-- DIRECT CHAT -->
                <div class="box direct-chat direct-chat-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Direct Chat</h3>
                        <div class="box-tools pull-right">
                            <span data-toggle="tooltip" title="2 New Messages" class="badge bg-green">2</span>
                            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                            <i class="fa fa-comments"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Conversations are loaded here -->
                        <div class="direct-chat-messages inner-content-div">
                            <!-- Message. Default to the left -->
                            <div class="direct-chat-msg">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left">James Anderson</span>
                                    <span class="direct-chat-timestamp pull-right">April 14, 2017 18:00 </span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="images/user1-128x128.jpg" alt="message user image">
                                <!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    Lorem Ipsum is simply dummy text of the printing and type setting industry.
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->
                            <!-- Message to the right -->
                            <div class="direct-chat-msg right">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-right">Michael Jorden</span>
                                    <span class="direct-chat-timestamp pull-left">April 14, 2017 18:00</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="images/user3-128x128.jpg" alt="message user image">
                                <!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    Lorem Ipsum is simply dummy text...
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->
                            <!-- Message. Default to the left -->
                            <div class="direct-chat-msg">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left">Johnathan Doeting</span>
                                    <span class="direct-chat-timestamp pull-right">April 14, 2017 18:00</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="images/user1-128x128.jpg" alt="message user image">
                                <!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem Ipsum is simply dummy text...
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->
                            <!-- Message to the right -->
                            <div class="direct-chat-msg right">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-right">Michael Jorden</span>
                                    <span class="direct-chat-timestamp pull-left">April 14, 2017 18:00</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="images/user3-128x128.jpg" alt="message user image">
                                <!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    Lorem Ipsum is simply dummy text of the printing and type setting industry. 
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->
                        </div>
                        <!--/.direct-chat-messages-->
                        <!-- Contacts are loaded here -->
                        <div class="direct-chat-contacts">
                            <div class="clearfix inner-content-div">
                                <ul class="contacts-list">
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="images/user1-128x128.jpg" alt="User Image">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                Pavan kumar
                                                <small class="contacts-list-date pull-right">April 14, 2017</small>
                                                </span>
                                                <span class="contacts-list-email">info@.multipurposethemes.com</span>
                                            </div>
                                            <!-- /.contacts-list-info -->
                                        </a>
                                    </li>
                                    <!-- End Contact Item -->
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="images/user7-128x128.jpg" alt="User Image">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                Sonu Sud
                                                <small class="contacts-list-date pull-right">March 14, 2017</small>
                                                </span>
                                                <span class="contacts-list-email">sonu@gmail.com</span>
                                            </div>
                                            <!-- /.contacts-list-info -->
                                        </a>
                                    </li>
                                    <!-- End Contact Item -->
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="images/user3-128x128.jpg" alt="User Image">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                Arijit Khan
                                                <small class="contacts-list-date pull-right">March 14, 2017</small>
                                                </span>
                                                <span class="contacts-list-email">arji898@gmail.com</span>
                                            </div>
                                            <!-- /.contacts-list-info -->
                                        </a>
                                    </li>
                                    <!-- End Contact Item -->
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="images/user5-128x128.jpg" alt="User Image">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                Joney Doe
                                                <small class="contacts-list-date pull-right">February 14, 2017</small>
                                                </span>
                                                <span class="contacts-list-email">jone148@yahoo.com</span>
                                            </div>
                                            <!-- /.contacts-list-info -->
                                        </a>
                                    </li>
                                    <!-- End Contact Item -->
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="images/user6-128x128.jpg" alt="User Image">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                Rokey Mehar
                                                <small class="contacts-list-date pull-right">February 14, 2017</small>
                                                </span>
                                                <span class="contacts-list-email">rok487@yahoo.com</span>
                                            </div>
                                            <!-- /.contacts-list-info -->
                                        </a>
                                    </li>
                                    <!-- End Contact Item -->
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="images/user8-128x128.jpg" alt="User Image">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                Akshay Kumar
                                                <small class="contacts-list-date pull-right">February 14, 2017</small>
                                                </span>
                                                <span class="contacts-list-email">aki489@gmail.com</span>
                                            </div>
                                            <!-- /.contacts-list-info -->
                                        </a>
                                    </li>
                                    <!-- End Contact Item -->
                                </ul>
                            </div>
                            <!-- /.contatcts-list -->
                        </div>
                        <!-- /.direct-chat-pane -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <form action="#" method="post">
                            <div class="input-group">
                                <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                <span class="input-group-btn">
                                <button type="button" class="btn bg-green btn-flat">Send</button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!--/.direct-chat -->
                <!-- USERS LIST -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">New User</h3>
                        <div class="box-tools pull-right">
                            <span class="label bg-aqua">8 New User</span>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <ul class="users-list clearfix">
                            <li>
                                <img src="images/user1-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Arijit Sinh</a>
                                <span class="users-list-date">Today</span>
                            </li>
                            <li>
                                <img src="images/user8-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Sonu Nigam</a>
                                <span class="users-list-date">Yesterday</span>
                            </li>
                            <li>
                                <img src="images/user7-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Pavan kumar</a>
                                <span class="users-list-date">12 Jan</span>
                            </li>
                            <li>
                                <img src="images/user6-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">John Doe</a>
                                <span class="users-list-date">12 Jan</span>
                            </li>
                            <li>
                                <img src="images/user2-160x160.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Alexander</a>
                                <span class="users-list-date">13 Jan</span>
                            </li>
                            <li>
                                <img src="images/user5-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Angela</a>
                                <span class="users-list-date">14 Jan</span>
                            </li>
                            <li>
                                <img src="images/user4-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Maical</a>
                                <span class="users-list-date">15 Jan</span>
                            </li>
                            <li>
                                <img src="images/user3-128x128.jpg" alt="User Image">
                                <a class="users-list-name" href="#">Juliya</a>
                                <span class="users-list-date">15 Jan</span>
                            </li>
                        </ul>
                        <!-- /.users-list -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="javascript:void(0)" class="uppercase">View All Users</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!--/.box -->
                <!-- Info Boxes Style 2 -->
                <div class="info-box bg-blue">
                    <span class="info-box-icon push-bottom"><i class="ion ion-ios-pricetag-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">New Clients</span>
                        <span class="info-box-number">450</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 45%"></div>
                        </div>
                        <span class="progress-description">
                        45% Increase in 28 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                <div class="info-box bg-green">
                    <span class="info-box-icon push-bottom"><i class="ion ion-ios-eye-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Visits</span>
                        <span class="info-box-number">15,489</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 40%"></div>
                        </div>
                        <span class="progress-description">
                        40% Increase in 28 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                <div class="info-box bg-purple">
                    <span class="info-box-icon push-bottom"><i class="ion ion-ios-cloud-download-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Downloads</span>
                        <span class="info-box-number">55,005</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 85%"></div>
                        </div>
                        <span class="progress-description">
                        85% Increase in 28 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                <div class="info-box bg-red">
                    <span class="info-box-icon push-bottom"><i class="ion-ios-chatbubble-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Direct Chat</span>
                        <span class="info-box-number">13,921</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 50%"></div>
                        </div>
                        <span class="progress-description">
                        50% Increase in 28 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>  --}}
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@push('dynamic_scripts')
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('assets/vendor_components/jquery-ui/jquery-ui.js') }}"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Sparkline -->
<script src="{{ asset('assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js') }}"></script>

<!-- jvectormap -->
<script src="{{ asset('assets/vendor_plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>  

<!-- ChartJS -->
<script src="{{ asset('assets/vendor_components/chart-js/chart.js') }}"></script>

<!-- mpt_admin dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('js/pages/dashboard2.js') }}"></script>
@endpush