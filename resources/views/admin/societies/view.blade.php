@extends('admin.layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{$society->socName}}
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/societies') }}">Societies</a></li>
            <li class="breadcrumb-item active">View Society Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Basic Forms -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Society Details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group row">
                            <label for="society_name" class="col-sm-3 col-form-label">Society Name</label>
                            <div class="col-sm-9">
                                {{$society->socName}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="society_registration_number" class="col-sm-3 col-form-label">Society Registration Number</label>
                            <div class="col-sm-9">
                                {{$society->socRegNo}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gst_number" class="col-sm-3 col-form-label">GSTin Number</label>
                            <div class="col-sm-9">
                                {{$society->socGSTNo}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cts_number" class="col-sm-3 col-form-label">CTS Number</label>
                            <div class="col-sm-9">
                                {{$society->socCTSNo}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="society_address" class="col-sm-3 col-form-label">Society Address</label>
                            <div class="col-sm-9">
                                {{$society->socAddress}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="society_city" class="col-sm-3 col-form-label">City</label>
                            <div class="col-sm-9">
                                {{$society->socCity}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="society_state" class="col-sm-3 col-form-label">State</label>
                            <div class="col-sm-9">
                                {{$society->socState}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="society_pincode" class="col-sm-3 col-form-label">Pincode</label>
                            <div class="col-sm-9">
                                {{$society->socPin}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="society_email" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                {{$society->socEmail}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="society_telephone_number" class="col-sm-3 col-form-label">Office Contact Number</label>
                            <div class="col-sm-9">
                                {{$society->socContact}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="number_of_tenants" class="col-sm-3 col-form-label">Total Number of Tenants</label>
                            <div class="col-sm-9">
                                {{$society->socTenantCnt}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="number_of_residential_flats" class="col-sm-3 col-form-label">Total Number of Residential Flats</label>
                            <div class="col-sm-9">
                                {{$society->socResFlatsCnt}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="number_of_shops" class="col-sm-3 col-form-label">Total Number of Shops</label>
                            <div class="col-sm-9">
                                {{$society->socShopsCnt}}
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <a href="{{ url('/societies') }}" class="btn btn-primary pull-right">Go Back</a>
            </div>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection