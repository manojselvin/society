@extends('admin.layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Update Society
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/societies') }}">Societies</a></li>
            <li class="breadcrumb-item active">Update Society</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Basic Forms -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Society Details Form</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <form action="/societies/{{$society->id}}" method="post" id="update_society_form">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row">
                                <label for="socName" class="col-sm-3 col-form-label">Society Name</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" name="socName" id="socName" value="{{$society->socName}}" placeholder="ABS Co-op. Hsg. Soc. Ltd. ">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socRegNo" class="col-sm-3 col-form-label">Society Registration Number</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" value="{{$society->socRegNo}}" name="socRegNo" id="socRegNo" placeholder="MAH000000SSX89">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socGSTNo" class="col-sm-3 col-form-label">GSTin Number</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" value="{{$society->socGSTNo}}" name="socGSTNo" id="socGSTNo" placeholder="27ABCPG7878E9878">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socCTSNo" class="col-sm-3 col-form-label">CTS Number</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" value="{{$society->socCTSNo}}" name="socCTSNo" id="socCTSNo" placeholder="27ABCPG7878E9878">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socAddress" class="col-sm-3 col-form-label">Society Address</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="socAddress" id="socAddress" placeholder="Goregaon W">{{$society->socAddress}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socCity" class="col-sm-3 col-form-label">City</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" value="{{$society->socCity}}" name="socCity" id="socCity" placeholder="Mumbai">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socState" class="col-sm-3 col-form-label">State</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" value="{{$society->socState}}" name="socState" id="socState" placeholder="Maharashtra">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socPin" class="col-sm-3 col-form-label">Pincode</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" value="{{$society->socPin}}" name="socPin" id="socPin" placeholder="400001">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socEmail" class="col-sm-3 col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="email" name="socEmail" id="socEmail" placeholder="society@gmail.com" value="{{$society->socEmail}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socContact" class="col-sm-3 col-form-label">Office Contact Number</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="tel" name="socContact" id="socContact" placeholder="+91 22 2874 5522" value="{{$society->socContact}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socTenantCnt" class="col-sm-3 col-form-label">Total Number of Tenants</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="number" id="socTenantCnt" name="socTenantCnt" value="{{$society->socTenantCnt}}" placeholder="65">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socResFlatsCnt" class="col-sm-3 col-form-label">Total Number of Residential Flats</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="number" id="socResFlatsCnt" name="socResFlatsCnt" value="{{$society->socResFlatsCnt}}" placeholder="35">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="socShopsCnt" class="col-sm-3 col-form-label">Total Number of Shops</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="number" id="socShopsCnt" name="socShopsCnt" value="{{$society->socShopsCnt}}" placeholder="15">
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="button" onclick="goBack()" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Update</button>
                </div>
            </form>
            <!-- /.form -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection