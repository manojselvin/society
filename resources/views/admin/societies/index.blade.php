@extends('admin.layouts.master')

@push('custom_stylesheets')

@endpush


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Societies
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="breadcrumb-item active">Societies</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List of Societies</h3>
                        <a href="{{ url('societies/create') }}" class="btn btn-success btn-sm float-right">
                            <i class="fa fa-plus"></i> Add New Society
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @include('admin.layouts.messages')
                        <table id="example" class=  "table table-bordered table-hover display nowrap margin-top-10 table-responsive">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($societies)
                                    @foreach($societies as $society)
                                        <tr>
                                            <td>{{$society->socCode}}</td>
                                            <td>{{$society->socName}}</td>
                                            <td>{{$society->socAddress}}</td>
                                            <td>{{$society->socEmail}}</td>
                                            <td>{{$society->socContact}}</td>
                                            <td>{{$society->created_at->diffForHumans()}}</td>
                                            <td>{{$society->updated_at->diffForHumans()}}</td>
                                            <td>
                                                <a href="societies/{{$society->id}}" class="btn btn-info btn-flat">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a href="societies/{{$society->id}}/edit" class="btn btn-info btn-flat">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <form action="/societies/{{$society->id}}" method="post" style="    display: inline-block;">
                                                    {{ csrf_field() }} {{ method_field('DELETE') }}
                                                    <button type="submit" class="btn btn-danger btn-flat" id="delete_society">
                                                        <i class="fa fa-trash-o"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->          
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@push('dynamic_scripts')
<!-- DataTables -->
<script src="{{ asset('assets/vendor_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<!-- This is data table -->
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

<!-- start - This is for export functionality only -->
{{--  <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>  --}}
<!-- end - This is for export functionality only -->

<!-- bonitoadmin for Data Table -->
<script src="{{ asset('js/pages/data-table.js') }}"></script>
@endpush

@push('inline_script')
<script>
!function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples 
    SweetAlert.prototype.init = function() {
        $('#delete_society').click(function(){
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this society again!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                cancelButtonText: "No, cancel please!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {     
                    swal("Deleted!", "Society has been deleted.", "success");   
                } else {     
                    swal("Cancelled", "Your society is safe :)", "error");   
                } 
            });
        });
    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.SweetAlert.init()
}(window.jQuery);
</script>
@endpush