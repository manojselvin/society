<footer class="main-footer">
<div class="pull-right d-none d-sm-inline-block">
  <b>Version</b> 0.1
</div>Copyright &copy; 2017 <a href="https://www.webgeeks.in/">Webgeeks</a>. All Rights Reserved.
</footer>

</div>
<!-- ./wrapper -->

<!-- All Js Files start -->
<!-- jQuery 3 -->
<script src="{{ asset('assets/vendor_components/jquery/dist/jquery.js') }}"></script>

<!-- popper -->
<script src="{{ asset('assets/vendor_components/popper/dist/popper.min.js') }}"></script>

<!-- Bootstrap 4.0 -->
<script src="{{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.js') }}"></script>  

<!-- Slimscroll -->
<script src="{{ asset('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

<!-- mpt_admin App -->
<script src="{{ asset('js/template.js') }}"></script>

<!-- mpt_admin for demo purposes -->
<script src="{{ asset('js/demo.js') }}"></script>

<!-- SweetAlert -->
<script src="{{ asset('assets/vendor_components/sweetalert/sweetalert.min.js') }}"></script>

<!-- Page Wise Dynamic JS -->
@stack('dynamic_scripts')

<!-- Inline JS -->
@stack('inline_script')

<!-- All Js Files end -->
