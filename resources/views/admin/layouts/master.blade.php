<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">

    <title>{{config('app.name','Smart Society System')}}</title>

	<!-- bootstrap 4.0 -->
	<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap/dist/css/bootstrap.css') }}">

	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css') }}">

	<!-- font awesome -->
	<link rel="stylesheet" href="{{ asset('assets/vendor_components/font-awesome/css/font-awesome.css') }}">

	<!-- ionicons -->
	<link rel="stylesheet" href="{{ asset('assets/vendor_components/Ionicons/css/ionicons.css') }}">

	<!-- theme style -->
    <link rel="stylesheet" href="{{ asset('css/master_style.css') }}">
    
    <!--alerts CSS -->
    <link href="{{ asset('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

	<!-- mpt_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.css') }}">
    
    <!-- Page Wise Dynamic CSS -->
    @stack('dynamic_stylesheets')

    <!-- Inline CSS -->
    @stack('inline_css')

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- google font -->
	<link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  </head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
        <!-- Header Navigation Menu -->
        @include('admin.layouts.nav')

        <!-- Sidebar Navigation Menu -->
		@include('admin.layouts.sidebar')

		<!-- Main Page Content -->
        @yield('content')

        <!-- Footer Page -->
        @include('admin.layouts.footer')

  </body>
</html>
