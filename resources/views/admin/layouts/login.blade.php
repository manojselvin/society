<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="{{ asset('images/favicon.ico') }}">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- bootstrap 4.0 -->
        <link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap/dist/css/bootstrap.css') }}">
        <!-- Bootstrap 4.0-->
        <link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css') }}">
        <!-- font awesome -->
        <link rel="stylesheet" href="{{ asset('assets/vendor_components/font-awesome/css/font-awesome.css') }}">
        <!-- ionicons -->
        <link rel="stylesheet" href="{{ asset('assets/vendor_components/Ionicons/css/ionicons.css') }}">
        <!-- theme style -->
        <link rel="stylesheet" href="{{ asset('css/master_style.css') }}">
        <!-- mpt_admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.css') }}">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- google font -->
        <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="{{ url('/') }}"><b>Smart Society System</b></a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <!-- Login Form -->
                @yield('content')
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- All Js Files start -->
        <!-- jQuery 3 -->
        <script src="{{ asset('assets/vendor_components/jquery/dist/jquery.js') }}"></script>
        <!-- popper -->
        <script src="{{ asset('assets/vendor_components/popper/dist/popper.min.js') }}"></script>
        <!-- Bootstrap 4.0 -->
        <script src="{{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.js') }}"></script>
    </body>
</html>