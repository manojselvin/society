@extends('admin.layouts.login')

@section('content')
<p class="login-box-msg">Sign in to start your session</p>
<form action="{{ url('/dashboard') }}" method="get" class="form-element">
    <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email">
        <span class="ion ion-email form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password">
        <span class="ion ion-locked form-control-feedback"></span>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="checkbox">
                <input type="checkbox" id="basic_checkbox_1">
                <label for="basic_checkbox_1">Remember Me</label>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-6">
            <div class="fog-pwd">
                <a href="javascript:void(0)"><i class="ion ion-locked"></i> Forgot pwd?</a><br>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-12 text-center">
            <button type="submit" class="btn btn-info btn-block btn-flat margin-top-10">SIGN IN</button>
        </div>
        <!-- /.col -->
    </div>
</form>
@endsection