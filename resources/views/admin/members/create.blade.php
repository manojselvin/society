@extends('admin.layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add New Member
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('members') }}">Members</a></li>
            <li class="breadcrumb-item active">Add New Members</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Basic Forms -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Member Details Form</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <form action="{{ route('members') }}" method="post" id="create_member_form" >
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="#">
                        <!-- Step 1 -->
                        <h4>Society Info</h4>
                        <section>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="firstName5">Full Name :</label>
                                        <input type="text" class="form-control" id="firstName5"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="firstName5">Members Role in Society</label>
                                        <select class="custom-select form-control" id="member_roles" name="member_roles">
                                            <option value="">Select Role</option>
                                            <option value="1">Chairman</option>
                                            <option value="2">Secretary</option>
                                            <option value="3">Treasurer</option>
                                            <option value="4">Joint Secretary</option>
                                            <option value="5">Vice Chairman</option>
                                            <option value="6">Committee Member</option>
                                            <option value="7">Society Member</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="firstName5">Society Name</label>
                                        <select class="custom-select form-control" id="member_roles" name="member_roles">
                                            <option value="">Select Society</option>
                                            <option value="1">ABS Co-op. Hsg. Soc. Ltd.</option>
                                            <option value="2">BRS Co-op. Hsg. Soc. Ltd.</option>
                                            <option value="3">TTTTT Co-op. Hsg. Soc. Ltd.</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="lastName1">Wing No :</label>
                                        <input type="text" class="form-control" id="lastName1"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="emailAddress1">Flat / Shop No :</label>
                                        <input type="email" class="form-control" id="emailAddress1"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="phoneNumber1">Floor No :</label>
                                        <input type="tel" class="form-control" id="phoneNumber1"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="emailAddress1">Type</label>
                                        <select class="custom-select form-control" id="location3" name="location">
                                            <option value="">Select Type</option>
                                            <option value="residential">Residential</option>
                                            <option value="shop">Shop</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="phoneNumber1">Unit Type :</label>
                                        <select class="custom-select form-control" id="location3" name="location">
                                            <option value="">Select Unit Type</option>
                                            <option value="1rk">1 RK</option>
                                            <option value="1bhk">1 BHK</option>
                                            <option value="2bhk">2 BHK</option>
                                            <option value="3bhk">3 BHK</option>
                                            <option value="4bhk">4 BHK</option>
                                            <option value="shop">Shop</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="emailAddress1">Carpet Area :</label>
                                        <input type="text" class="form-control" id="emailAddress1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="emailAddress1">Number of Occupants :</label>
                                        <input type="text" class="form-control" id="emailAddress1"> 
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!-- Step 2 -->
                        <h4>Personal Info</h4>
                        <section>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="emailAddress1">Email Address :</label>
                                        <input type="email" class="form-control" id="emailAddress1"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="phoneNumber1">Mobile Number :</label>
                                        <input type="tel" class="form-control" id="phoneNumber1"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="date1">Date of Birth :</label>
                                        <input type="date" class="form-control" id="date1"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="emailAddress1">Pan Card Number :</label>
                                        <input type="text" class="form-control" id="emailAddress1"> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="emailAddress1">Aadhar Card Number :</label>
                                        <input type="text" class="form-control" id="emailAddress1"> 
                                    </div>
                                </div>
                            </div>
                        </section>
                        
                        <!-- Step 3 -->
                        <h4>Profession Info</h4>
                        <section>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jobTitle1">Profession :</label>
                                        <select class="custom-select form-control" id="Location1" name="location">
                                            <option value="">Select Profession</option>
                                            <option value="1">Service</option>
                                            <option value="2">Business</option>
                                            <option value="3">Housewife</option>
                                            <option value="4">Freelancer</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="videoUrl1">Organization Name :</label>
                                        <input type="text" class="form-control" id="videoUrl1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="videoUrl1">Work Email :</label>
                                        <input type="email" class="form-control" id="videoUrl1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jobTitle1">Office Contact Number :</label>
                                        <input type="tel" class="form-control" id="videoUrl1">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="videoUrl1">Address :</label>
                                        <input type="text" class="form-control" id="videoUrl1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jobTitle1">City :</label>
                                        <input type="text" class="form-control" id="videoUrl1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="videoUrl1">State :</label>
                                        <input type="text" class="form-control" id="videoUrl1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="videoUrl1">Pincode :</label>
                                        <input type="text" class="form-control" id="videoUrl1">
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!-- Step 4 -->
                        <h4>Family Member Details</h4>
                        <section>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Full Name :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Relation :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Mobile Number :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="jobTitle2">E-mail Address :</label>
                                        <input type="text" class="form-control" id="jobTitle2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Full Name :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Relation :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Mobile Number :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="jobTitle2">E-mail Address :</label>
                                        <input type="text" class="form-control" id="jobTitle2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Full Name :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Relation :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Mobile Number :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="jobTitle2">E-mail Address :</label>
                                        <input type="text" class="form-control" id="jobTitle2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Full Name :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Relation :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Mobile Number :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="jobTitle2">E-mail Address :</label>
                                        <input type="text" class="form-control" id="jobTitle2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Full Name :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Relation :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Mobile Number :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="jobTitle2">E-mail Address :</label>
                                        <input type="text" class="form-control" id="jobTitle2">
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!-- Step 5 -->
                        <h4>Vehicle Details</h4>
                        <section>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Car/Bike Model :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Registration Number :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Parking Alloted :</label>
                                        <select class="custom-select form-control" id="Location1" name="location">
                                            <option value="">Select Option</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="jobTitle2">Parking No :</label>
                                        <input type="text" class="form-control" id="jobTitle2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Car/Bike Model :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Registration Number :</label>
                                        <input type="text" class="form-control" id="int1"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int1">Parking Alloted :</label>
                                        <select class="custom-select form-control" id="Location1" name="location">
                                            <option value="">Select Option</option>
                                            <option value="1">Yes</option>
                                            <option value="2">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="jobTitle2">Parking No :</label>
                                        <input type="text" class="form-control" id="jobTitle2">
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="button" onclick="goBack()" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                </div>
                <!-- /.box-footer -->
            </form>
            <!-- /.form -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@push('dynamic_scripts')

@endpush