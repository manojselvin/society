@extends('admin.layouts.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add New Service
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('services') }}">Services</a></li>
            <li class="breadcrumb-item active">Add New Service</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Basic Forms -->
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Service Details Form</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
            </div>
            <form action="{{ url('services/update') }}" method="post" id="create_service_form" >
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="#">
                        <!-- Step 1 -->
                        <h4>Service Info</h4>
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="serviceName">Service Name :</label>
                                        <input type="text" class="form-control" id="serviceName" value="{{ isset($service->srName) ? $service->srName : '' }}" /> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="serviceShortCode">Service Short Code :</label>
                                        <input type="text" class="form-control" id="serviceShortCode" value="{{ isset($service->srCode) ? $service->srCode : '' }}" /> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="serviceStatus">Service Initial Status</label>
                                        <select class="custom-select form-control" id="s" name="serviceStatus">
                                            <option value="">Select Role</option>
                                            <option value="1" {{ isset($service->activeStatus) and $service->activeStatus == 1 ? 'selected' : '' }}>Active</option>
                                            <option value="0" {{ isset($service->activeStatus) and $service->activeStatus == 0 ? 'selected' : '' }}>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </section>
                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="button" onclick="goBack()" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                </div>
                <!-- /.box-footer -->
            </form>
            <!-- /.form -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@push('dynamic_scripts')

@endpush