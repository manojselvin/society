@extends('admin.layouts.master')

@push('custom_stylesheets')

@endpush


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Services
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="breadcrumb-item active">Services</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">List of Services</h3>
                        <a href="{{ url('services/create') }}" class="btn btn-success btn-sm float-right">
                            <i class="fa fa-plus"></i> Add New Service
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive">
                            <thead>
                                <tr>
                                    <th>Service Name</th>
                                    <th>Service Short Code</th>
                                    <th>Service Status</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($services as $single_service)
                                <tr>
                                    <td>{{ $single_service->srName }}</td>
                                    <td>{{ $single_service->srCode }}</td>
                                    <td>{{ $single_service->activeStatus }}</td>
                                    <td>{{ $single_service->created_at->diffForHumans() }}</td>
                                    <td>{{ $single_service->updated_at->diffForHumans() }}</td>
                                    <td>
                                        <a href="{{ url('services/'.$single_service->id.'/update') }}" class="btn btn-info btn-flat">
                                            <i class="fa fa-pencil"></i> Edit
                                        </a>
                                        <a href="javascript:;" class="btn btn-danger btn-flat" id="delete_member">
                                            <i class="fa fa-trash-o"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->          
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@push('dynamic_scripts')
<!-- DataTables -->
<script src="{{ asset('assets/vendor_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<!-- This is data table -->
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

<!-- start - This is for export functionality only -->
{{--  <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>  --}}
<!-- end - This is for export functionality only -->

<!-- bonitoadmin for Data Table -->
<script src="{{ asset('js/pages/data-table.js') }}"></script>
@endpush

@push('inline_script')
<script>
!function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples 
    SweetAlert.prototype.init = function() {
        $('#delete_member').click(function(){
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this member again!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                cancelButtonText: "No, cancel please!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {     
                    swal("Deleted!", "Member has been deleted.", "success");   
                } else {     
                    swal("Cancelled", "Your member is safe :)", "error");   
                } 
            });
        });
    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.SweetAlert.init()
}(window.jQuery);
</script>
@endpush