<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Society extends Model
{
    //To Make a Soft Delete Work
    use SoftDeletes;

    protected $table = 'societies';

    protected $fillable = [
        'socCode',
        'socName',
        'socRegNo',
        'socAddress',
        'socCity',
        'socState',
        'socPin',
        'socEmail',
        'socContact',
        'socTenantCnt',
        'socResFlatsCnt',
        'socShopsCnt',
        'socGSTNo',
        'socCTSNo',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
