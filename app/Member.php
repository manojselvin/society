<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    //To Make a Soft Delete Work
    use SoftDeletes;

    protected $table = 'members';

    protected $fillable = [
        'memName',
        'memSocID',
        'memRoleID',
        'memWingNo',
        'memFlatNo',
        'memFloorNo',
        'memFlatType',
        'memFlatUnitType',
        'memCarpetArea',
        'memNoOfOccupants',
        'memEmail',
        'memContact',
        'memDOB',
        'memPAN',
        'memAdhaar',
        'memProf',
        'memOrgName',
        'memWorkEmail',
        'memOffContact',
        'memOffAddr',
        'memOffCity',
        'memOffState',
        'memOffPinCode',
        'memFamilyDetails',
        'memVehicleInfo',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
