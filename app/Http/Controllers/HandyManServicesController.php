<?php

namespace App\Http\Controllers;

use App\HandyManServices;
use Illuminate\Http\Request;

class HandyManServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['services'] = self::getServiceList();

        return view('admin.services.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HandyManServices  $handyManServices
     * @return \Illuminate\Http\Response
     */
    public function show(HandyManServices $handyManServices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HandyManServices  $handyManServices
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['service'] = self::getService($id);
        return view('admin.services.create', $data);
    }

    public function getServiceList()
    {
        return HandyManServices::all();
    }

    public function getService($id)
    {
        return HandyManServices::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HandyManServices  $handyManServices
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, HandyManServices $handyManServices)
    {
      dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HandyManServices  $handyManServices
     * @return \Illuminate\Http\Response
     */
    public function destroy(HandyManServices $handyManServices)
    {
        //
    }
}
