<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Society;

class SocietyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get All Societies From DB
        $societies = Society::all();

        //Pass Data into View
        return view('admin.societies.index',compact('societies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.societies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //When in pain turn me ON
        //dd($request->all()); //Dump and Die

        //Field Validations
        $this->validate(request(),[
            'socName' => 'required',
            'socRegNo' => 'required',
            'socAddress' => 'required',
            'socCity' => 'required',
            'socState' => 'required',
            'socPin' => 'required',
            'socEmail' => 'required',
            'socContact' => 'required',
            'socTenantCnt' => 'required',
            'socResFlatsCnt' => 'required',
            'socShopsCnt' => 'required',
            'socGSTNo' => 'required',
            'socCTSNo' => 'required',
        ]);
        
        //Push it in to DB
        Society::create($request->all());

        //After Successful insert redirect to Index
        return redirect('/societies')->with('success', 'Society Registration Successful.');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Get Data Based on ID
        return view('admin.societies.view',['society' => Society::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('admin.societies.update',['society' => Society::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //When in pain turn me ON
        //dd($request->all()); //Dump and Die

        //Field Validations
        $this->validate(request(),[
            'socName' => 'required',
            'socRegNo' => 'required',
            'socAddress' => 'required',
            'socCity' => 'required',
            'socState' => 'required',
            'socPin' => 'required',
            'socEmail' => 'required',
            'socContact' => 'required',
            'socTenantCnt' => 'required',
            'socResFlatsCnt' => 'required',
            'socShopsCnt' => 'required',
            'socGSTNo' => 'required',
            'socCTSNo' => 'required',
        ]);
        
        //Update it in to DB
        $society = Society::find($id);
        $society->save($request->all());

        //After Successful insert redirect to Index
        return redirect('/societies')->with('success', 'Society Updation Successful.'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Soft Delete it
        $society = Society::find($id);
        $society->delete();

        //After Successful insert redirect to Index
        return redirect('/societies')->with('success', 'Society Deletion Successful.'); 
    }
}
