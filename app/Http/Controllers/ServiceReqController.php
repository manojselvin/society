<?php

namespace App\Http\Controllers;

use App\ServiceReq;
use Illuminate\Http\Request;

class ServiceReqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    // Get info around a single member
    public function getAllReq($memberID)
    {
        return ServiceReq::where('memID', $memberID)->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceReq  $serviceReq
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceReq $serviceReq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceReq  $serviceReq
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceReq $serviceReq)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceReq  $serviceReq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceReq $serviceReq)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceReq  $serviceReq
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceReq $serviceReq)
    {
        //
    }
}
