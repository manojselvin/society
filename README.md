# README #

### What is this repository for? ###
This is a Society Management System designed for Yocket.
* Version: 1.0

### How do I get set up? ###
1. clone the repo
2. create a database with name society
3. modify the .env file with your db username and password
4. open cmd cd into the cloned dir Society
5. run "composer install" to install dependency
6. run "composer dump autoload"
7. run "php artisan:migrate" to start db table migrations which are included.
8. run "php artisan db:seed" to seed the table with data.
9. run "php artisan key:generate" to generate and set the app encryption key.
10. run apache server and mysql server.
11. navigate to the folder from browser eg. http://localhost/Society/public/ to view the login page

### usage guidelines ###

1. Login using following credentials
username : manojselvin@gmail.com
password : admin123

### Who do I talk to? ###
author : Manoj Selvin
email : manojselvin@gmail.com
contact no : 9702437247