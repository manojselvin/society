<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*-----------------------------------------------------
|   Route For : Authentication
------------------------------------------------------*/
Route::get('/', function () {
    return view('admin.auth.login');
})->name('login');

Route::get('/logout', function () {
    return view('admin.auth.login');
})->name('logout');

/*-----------------------------------------------------
|   Route For : Dashboard
------------------------------------------------------*/
Route::get('/dashboard', function () {
    return view('admin.dashboard.statistics');
})->name('dashboard');


/*-----------------------------------------------------
|   Route For : Societies
------------------------------------------------------*/

Route::resource('societies', 'SocietyController');


/*-----------------------------------------------------
|   Route For : Members
------------------------------------------------------*/
Route::get('/members', function () {
    return view('admin.members.index');
})->name('members');

Route::get('/members/create', function () {
    return view('admin.members.create');
})->name('members-create');

Route::get('/members/{id}/update', function ($id) {
    return view('admin.members.update')->with($id);
})->name('members-update');

/*-----------------------------------------------------
|   Route For : Handy Man Services
------------------------------------------------------*/
Route::get('/services', 'HandyManServicesController@index')->name('services');

Route::get('/services/create', function () {
    return view('admin.services.create');
})->name('services-create');

Route::get('/services/{id}/update', 'HandyManServicesController@edit')->name('services-update');

Route::post('/services/{id}/update', 'HandyManServicesController@update')->name('services-update-data');

